# Traudicode API

[![pipeline status](https://gitlab.com/phvien/traudicode-api/badges/master/pipeline.svg)](https://gitlab.com/phvien/traudicode-api/-/commits/master)
[![coverage report](https://gitlab.com/phvien/traudicode-api/badges/master/coverage.svg)](https://gitlab.com/phvien/traudicode-api/-/commits/master)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
