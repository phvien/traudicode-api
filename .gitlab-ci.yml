---
stages:
  - test
  - build
  - review
  - staging
  - production

variables:
  BASE_IMAGE: node:16.15-alpine
  APP_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
  GITLAB_REGISTRY: -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  HEROKU_REGISTRY: registry.heroku.com
  HEROKU_DOMAIN: herokuapp.com

  HEROKU_STAGING_APP: traudicode-staging
  HEROKU_STAGING_IMAGE: $HEROKU_REGISTRY/$HEROKU_STAGING_APP/web
  HEROKU_STAGING_LOGIN: -u _ -p $HEROKU_STAGING_API_KEY $HEROKU_REGISTRY

  HEROKU_PROD_APP: traudicode-prod
  HEROKU_PROD_IMAGE: $HEROKU_REGISTRY/$HEROKU_PROD_APP/web
  HEROKU_PROD_LOGIN: -u _ -p $HEROKU_PROD_API_KEY $HEROKU_REGISTRY

.base_template: &base
  image: $BASE_IMAGE

.deployment_template: &deployment
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login $GITLAB_REGISTRY
  script:
    - docker pull $APP_IMAGE
    - docker tag $APP_IMAGE $HEROKU_IMAGE
    - docker login $HEROKU_LOGIN
    - docker push $HEROKU_IMAGE
    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli container:release web --app $HEROKU_APP
    - echo "Listening at https://$HEROKU_APP.$HEROKU_DOMAIN/"

test_jest:
  <<: *base
  stage: test
  script:
    - yarn
    - yarn build
    - yarn test:e2e
    - yarn test:cov
  coverage: '/All\sfiles.*?\s+(\d+.\d+)/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/clover.xml

build_docker_image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login $GITLAB_REGISTRY
  script:
    - docker build -t $APP_IMAGE .
    - docker images
    - docker push $APP_IMAGE

deploy_heroku_review:
  image: docker:latest
  stage: review
  variables:
    HEROKU_LOGIN: $HEROKU_STAGING_LOGIN
    HEROKU_API_KEY: $HEROKU_STAGING_API_KEY
    HEROKU_REVIEW_APP: $CI_ENVIRONMENT_SLUG
    HEROKU_REVIEW_IMAGE: $HEROKU_REGISTRY/$HEROKU_REVIEW_APP/web
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$HEROKU_REVIEW_APP.$HEROKU_DOMAIN/
    on_stop: destroy_heroku_review
  services:
    - docker:dind
  before_script:
    - docker login $GITLAB_REGISTRY
  script:
    - docker pull $APP_IMAGE
    - docker tag $APP_IMAGE $HEROKU_REVIEW_IMAGE
    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli create $HEROKU_REVIEW_APP
    - docker login $HEROKU_LOGIN
    - docker push $HEROKU_REVIEW_IMAGE
    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli container:release web --app $HEROKU_REVIEW_APP
    - echo "Listening at https://$HEROKU_REVIEW_APP.$HEROKU_DOMAIN/"
  only:
    - /^feature-.*$/

destroy_heroku_review:
  image: docker:latest
  stage: review
  variables:
    GIT_STRATEGY: none
    HEROKU_REVIEW_APP: $CI_ENVIRONMENT_SLUG
    HEROKU_API_KEY: $HEROKU_STAGING_API_KEY
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  services:
    - docker:dind
  script:
    - docker run --rm -e HEROKU_API_KEY=$HEROKU_API_KEY wingrunr21/alpine-heroku-cli apps:destroy --app $HEROKU_REVIEW_APP --confirm $HEROKU_REVIEW_APP
    - echo "Destroyed app at https://$HEROKU_REVIEW_APP.$HEROKU_DOMAIN/"
  when: manual

deploy_heroku_staging:
  <<: *deployment
  stage: staging
  variables:
    HEROKU_IMAGE: $HEROKU_STAGING_IMAGE
    HEROKU_LOGIN: $HEROKU_STAGING_LOGIN
    HEROKU_API_KEY: $HEROKU_STAGING_API_KEY
    HEROKU_APP: $HEROKU_STAGING_APP
  environment:
    name: staging
    url: https://$HEROKU_APP.herokuapp.com/
  only:
    - master

deploy_heroku_prod:
  <<: *deployment
  stage: production
  variables:
    HEROKU_IMAGE: $HEROKU_PROD_IMAGE
    HEROKU_LOGIN: $HEROKU_PROD_LOGIN
    HEROKU_API_KEY: $HEROKU_PROD_API_KEY
    HEROKU_APP: $HEROKU_PROD_APP
  environment:
    name: production
    url: https://$HEROKU_APP.herokuapp.com/
  only:
    - master
  when: manual
