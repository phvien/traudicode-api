FROM node:16.15-alpine

WORKDIR /traudicode-api

ADD . .

RUN yarn 
RUN yarn cache clean
RUN yarn --force
RUN yarn build

ENV PORT = $PORT

CMD yarn start:prod